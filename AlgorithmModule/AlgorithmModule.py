import base64
import json
import os
import random
import string
import sys
import time

import matplotlib.pyplot as plt
import paho.mqtt.client as mqtt  # import the client1
import paho.mqtt.publish as publish

import skimage.io
import skimage.io
import tensorflow as tf

# Root directory of the project
Mask_RCNN_DIR=r"./TheAlgorithm/Mask_RCNN"
sys.path.append(Mask_RCNN_DIR)  # To find local version of the library
from mrcnn import visualize
import mrcnn.model as modellib

ROOT_DIR =r"./TheAlgorithm/TheCustomModel/Model"
sys.path.append(ROOT_DIR)
import custom

host = "platform.connectedindustry.net"
username_interface = "CERP_cnn_user_interface"
password_interface = "7c6cc6b6-dd0e-4f4b-87ad-8e72b4730b28"
username_algorithm = "CERP_cnn_algorithm"
password_algorithm = "ef1b1d5a-f47b-4457-8410-95977bcd6a60"
username_camera = "CERP_cnn_camera"
password_camera = "174cf22e-13eb-4861-98a7-72160914c346"
InterfaceChannel = "channels/ea711973-4c84-4916-9e1d-12ab22d19e66/messages/interface"
CameraChannel = "channels/ea711973-4c84-4916-9e1d-12ab22d19e66/messages/camera"
AlgorithmChannel = "channels/ea711973-4c84-4916-9e1d-12ab22d19e66/messages/algorithm"




class ClientMQTT:
    def __init__(self):
        self.startReceiving()
        # self.Process()

    def on_message(self, client, userdata, message):
        print("message topic=", message.topic)
        print("message qos=", message.qos)
        print("message retain flag=", message.retain)
        try:
            s1 = str(message.payload.decode("utf-8"))
            y = json.loads(s1)
            print(y)
            if y["cmd"] == "Process":
                with open(r"./TheAlgorithm/ProcessingImages/toProcess.jpg", "wb") as new_file:
                    new_file.write(base64.b64decode(y["img"]))
                myData = {'cmd': "msg" , 'message': "Algorithm receive the image and start to process it"}  # json
                auth1 = dict(username=username_algorithm, password=password_algorithm)
                publish.single(topic=AlgorithmChannel, payload=json.dumps(myData), hostname=host, port=1883, auth=auth1)
                self.Process()
                self.startReceiving()
            elif   y["cmd"] == "AreYouHere":
                myData = {'cmd': "msg", 'message': "Algorithm waiting .........."}  # json
                auth1 = dict(username=username_algorithm, password=password_algorithm)
                publish.single(topic=AlgorithmChannel, payload=json.dumps(myData), hostname=host, port=1883, auth=auth1)

        except Exception as ex:
            print(ex)

    def on_connect(self, client_1, userdata, flags, rc):
        if rc == 0:
            print("connected1 OK")
        else:
            print("Bad connection Returned code=", rc)

    def startReceiving(self):
        print("creating new instance")
        client = mqtt.Client("algorithm")  # create new instance
        client._port = 1883
        client._username = username_algorithm
        client._password = password_algorithm
        client.on_message = self.on_message  # attach function to callback
        client._on_connect = self.on_connect
        client.connect(host)  # connect to broker
        print("Subscribing to topic")
        client.subscribe(AlgorithmChannel)
        client.subscribe(InterfaceChannel)
        myData = {'cmd': "msg", 'message': "Algorithm waiting .........."}  # json
        client.publish(topic=AlgorithmChannel, payload=json.dumps(myData))
        print("Algorithm waiting ..........")
        client.loop_forever()  # start the loop


    def convertImageToJson(self, filename):
        picId = ''.join(random.choice(string.ascii_lowercase) for i in range(8))
        with open(filename, mode='rb') as file:
            img = file.read()
        encoded = base64.encodebytes(img).decode("utf-8")
        myData = {'cmd': "Processed", 'img': encoded}
        return myData

    def publish(self, myData, sleep=15):
        print("Algorithm publish the results")
        auth1 = dict(username=username_algorithm, password=password_algorithm)
        publish.single(topic=AlgorithmChannel, payload=json.dumps(myData), hostname=host, port=1883, auth=auth1)

    def Process(self):

        print("############# Start image Processing ##############")

        MODEL_DIR = os.path.join(ROOT_DIR, "logs")
        custom_WEIGHTS_PATH = "./TheAlgorithm/TheCustomModel/Model/mask_rcnn_defekt_0010.h5"

        config = custom.CustomConfig()
        custom_DIR =  "./TheAlgorithm/TheCustomModel/Model/customImages/"

        class InferenceConfig(config.__class__):
            # Run detection on one image at a time
            GPU_COUNT = 1
            IMAGES_PER_GPU = 1

        config = InferenceConfig()
        config.display()

        DEVICE = "/cpu:0"  # /cpu:0 or /gpu:0

        # Inspect the model in training or inference modes
        # values: 'inference' or 'training'
        # TODO: code for 'training' test mode not ready yet
        TEST_MODE = "inference"

        def get_ax(rows=1, cols=1, size=16):
            """Return a Matplotlib Axes array to be used in
            all visualizations in the notebook. Provide a
            central point to control graph sizes.

            Adjust the size attribute to control how big to render images
            """
            _, ax = plt.subplots(rows, cols, figsize=(size * cols, size * rows))
            return ax

        # ## Load Validation Dataset
        # Load validation dataset
        dataset = custom.CustomDataset()
        dataset.load_custom(custom_DIR, "val")

        # Must call before using the dataset
        dataset.prepare()

        # print("Images: {}\nClasses: {}".format(len(dataset.image_ids), dataset.class_names))
        with tf.device(DEVICE):
            model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR,
                                      config=config)

        print("Loading weights ", custom_WEIGHTS_PATH)
        model.load_weights(custom_WEIGHTS_PATH, by_name=True)

        # In[9]:

        from importlib import \
            reload  # was constantly changin the visualization, so I decided to reload it instead of notebook

        reload(visualize)

        image = skimage.io.imread("./TheAlgorithm/ProcessingImages/toProcess.jpg")
        # Run object detection
        print("######## start object detection")
        results = model.detect([image], verbose=1)
        print("######## End object detection")

        # Display results
        ax = get_ax(1)
        r = results[0]

        visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'],
                                    dataset.class_names, r['scores'], ax=ax)

        print("############# Finish image Processing ##############")

        # plt.show()

        # publish the results
        print("############# start sending the results  ##############")

        myData = self.convertImageToJson("./TheAlgorithm/ProcessingImages/Processed.jpg")  # json
        print(myData)
        self.publish(myData)

        print("############# Finish sending the results  ##############")

if str(tf.__version__).split('.')[0]=="1":
    ClientMQTT()
else:
    raise Exception("wrong tensorflow version : should be 1.x")