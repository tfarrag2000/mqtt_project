import os
import sys

import matplotlib.pyplot as plt
import skimage.io
import tensorflow as tf

# Root directory of the project
Mask_RCNN_DIR = os.path.abspath(r"../../Mask_RCNN")
sys.path.append(Mask_RCNN_DIR)  # To find local version of the library
from mrcnn import visualize
import mrcnn.model as modellib

ROOT_DIR = os.getcwd()
import custom

# get_ipython().run_line_magic('matplotlib', 'inline')

# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

custom_WEIGHTS_PATH = "mask_rcnn_defekt_0010.h5"  # TODO: update this path

# ## Configurations

# In[2]:


config = custom.CustomConfig()
custom_DIR = os.path.join(ROOT_DIR, "customImages")


# In[3]:


# Override the training configurations with a few
# changes for inferencing.
class InferenceConfig(config.__class__):
    # Run detection on one image at a time
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


config = InferenceConfig()
config.display()

# ## Notebook Preferences

# In[4]:


# Device to load the neural network on.
# Useful if you're training a model on the same 
# machine, in which case use CPU and leave the
# GPU for training.
DEVICE = "/cpu:0"  # /cpu:0 or /gpu:0

# Inspect the model in training or inference modes
# values: 'inference' or 'training'
# TODO: code for 'training' test mode not ready yet
TEST_MODE = "inference"


# In[5]:


def get_ax(rows=1, cols=1, size=16):
    """Return a Matplotlib Axes array to be used in
    all visualizations in the notebook. Provide a
    central point to control graph sizes.
    
    Adjust the size attribute to control how big to render images
    """
    _, ax = plt.subplots(rows, cols, figsize=(size * cols, size * rows))
    return ax


# ## Load Validation Dataset

# In[6]:


# Load validation dataset
dataset = custom.CustomDataset()
dataset.load_custom(custom_DIR, "val")

# Must call before using the dataset
dataset.prepare()

# print("Images: {}\nClasses: {}".format(len(dataset.image_ids), dataset.class_names))


# ## Load Model

# In[7]:


# Create model in inference mode
with tf.device(DEVICE):
    model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR,
                              config=config)

# In[8]:


# load the last model you trained
# weights_path = model.find_last()[1]

# Load weights
# print("Loading weights ", custom_WEIGHTS_PATH)
# model.load_weights(custom_WEIGHTS_PATH, by_name=True, exclude=[ "mrcnn_class_logits", "mrcnn_bbox_fc", "mrcnn_bbox", "mrcnn_mask"])

print("Loading weights ", custom_WEIGHTS_PATH)
model.load_weights(custom_WEIGHTS_PATH, by_name=True)

# In[9]:


from importlib import reload  # was constantly changin the visualization, so I decided to reload it instead of notebook

reload(visualize)

# # Run Detection on Images

# In[10]:


# i = 0


# In[11]:


# image_id = random.choice(dataset.image_ids)
# image_id = 0

# image, image_meta, gt_class_id, gt_bbox, gt_mask =    modellib.load_image_gt(dataset, config, image_id, use_mini_mask=False)
# info = dataset.image_info[image_id]
# print("image ID: {}.{} ({}) {}".format(info["source"], info["id"], image_id,
#                                       dataset.image_reference(image_id)))


image = skimage.io.imread('detection.jpg')
# Run object detection
results = model.detect([image], verbose=1)

# Display results
ax = get_ax(1)
r = results[0]

visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'],
                            dataset.class_names, r['scores'], ax=ax,
                            title="Prognose")

# log("gt_class_id", gt_class_id)
# log("gt_bbox", gt_bbox)
# log("gt_mask", gt_mask)

plt.show()
# In[ ]:


# In[ ]:
