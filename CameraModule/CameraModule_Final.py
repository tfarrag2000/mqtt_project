import base64
import json
import random
import string
import cv2 as cv2
import paho.mqtt.client as mqtt  # import the client1
import paho.mqtt.publish as publish


host = "platform.connectedindustry.net"
username_interface = "CERP_cnn_user_interface"
password_interface = "7c6cc6b6-dd0e-4f4b-87ad-8e72b4730b28"
username_algorithm = "CERP_cnn_algorithm"
password_algorithm = "ef1b1d5a-f47b-4457-8410-95977bcd6a60"
username_camera = "CERP_cnn_camera"
password_camera = "174cf22e-13eb-4861-98a7-72160914c346"
InterfaceChannel = "channels/ea711973-4c84-4916-9e1d-12ab22d19e66/messages/interface"
CameraChannel = "channels/ea711973-4c84-4916-9e1d-12ab22d19e66/messages/camera"
CupturedImagePath = "FromCamera.jpg"


class ClientMQTT:
    GUI = None
    client: mqtt = None

    def __init__(self, GUI=None):
        self.startReceiving(GUI)

    def on_message(self, client, userdata, message):
        print("message topic=", message.topic)
        print("message qos=", message.qos)
        print("message retain flag=", message.retain)
        try:
            s1 = str(message.payload.decode("utf-8"))
            print(s1)
            y = json.loads(s1)
            print(y)
            if y["cmd"] == "Capture":
                myData = {'cmd': "msg" , 'message': "Camera starts to capture an image"}  # json
                auth1 = dict(username=username_interface, password=password_interface)
                publish.single(topic=CameraChannel, payload=json.dumps(myData), hostname=host, port=1883, auth=auth1)

                print("camera receiving capture Command")
                # x = threading.Thread(target=CameraModule, args=(self.client,),daemon=True)
                # x.start()
                CameraModule(client)
            elif y["cmd"] == "AreYouHere":
                myData = {'cmd': "msg", 'message': "Camera waiting .........."}  # json
                auth1 = dict(username=username_interface, password=password_interface)
                publish.single(topic=CameraChannel, payload=json.dumps(myData), hostname=host, port=1883, auth=auth1)

            self.startReceiving()
        except Exception as ex:
            print(ex)

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            print("connected OK")
            client.on_message = self.on_message
        else:
            print("Bad connection Returned code=", rc)

    def startReceiving(self, GUI=None):
        print("creating new instance")
        self.client = mqtt.Client("camera33")  # create new instance
        self.client._port = 1883
        self.client._username = username_camera
        self.client._password = password_camera
        self.client.on_message = self.on_message  # attach function to callback
        self. client._on_connect = self.on_connect
        self. client.connect(host)  # connect to broker
        print("Subscribing to topic")
        self.client.subscribe(InterfaceChannel)
        myData = {'cmd': "msg", 'message': "Camera waiting .........."}  # json
        self.client.publish(topic=CameraChannel, payload=json.dumps(myData))
        print("Camera waiting ..........")
        self.client.loop_forever()  # start the loop


class CameraModule:
    client : mqtt  = None

    def __init__(self, clinet):
        self.client = clinet
        self.Capture()
        self.Publish()


    def Capture(self):
        videoCaptureObject = cv2.VideoCapture(0)
        _, frame = videoCaptureObject.read()
        if not cv2.imwrite(CupturedImagePath, frame):
            raise Exception("Could not write image")

        videoCaptureObject.release()
        cv2.destroyAllWindows()
        print("Capture a camera image ............ done")
        pass

    def on_publish(self, client, userdata, result):  # create function for callback
        print("data published " + result)
        pass

    def Publish(self):
        self.client.on_publish = self.on_publish
        myData = self.convertImageToJson()
        auth1 = dict(username=username_interface, password=password_interface)
        publish.single(topic=CameraChannel, payload=json.dumps(myData),hostname=host,port=1883,auth=auth1)
        print("publish the image ............ done")

        time.sleep(4)
        pass

    def convertImageToJson(self):
        picId = ''.join(random.choice(string.ascii_lowercase) for i in range(8))
        with open(CupturedImagePath, mode='rb') as file:
            img = file.read()
        encoded = base64.encodebytes(img).decode("utf-8")
        myData = {'cmd': "FromCamera", 'picId': picId, 'img': encoded}
        return myData


ClientMQTT()
