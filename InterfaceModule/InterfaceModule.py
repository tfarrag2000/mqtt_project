import base64
import json
import random
import string
import threading
import time
import tkinter as tk  # GUI Packege
from datetime import datetime
from shutil import copyfile
from tkinter import filedialog
import paho.mqtt.client as mqtt  # import the client1
import paho.mqtt.publish as publish
from PIL import Image
from PIL import ImageTk

host = "platform.connectedindustry.net"
username_interface = "CERP_cnn_user_interface"
password_interface = "7c6cc6b6-dd0e-4f4b-87ad-8e72b4730b28"
username_algorithm = "CERP_cnn_algorithm"
password_algorithm = "ef1b1d5a-f47b-4457-8410-95977bcd6a60"
username_camera = "CERP_cnn_camera"
password_camera = "174cf22e-13eb-4861-98a7-72160914c346"
InterfaceChannel = "channels/ea711973-4c84-4916-9e1d-12ab22d19e66/messages/interface"
CameraChannel = "channels/ea711973-4c84-4916-9e1d-12ab22d19e66/messages/camera"
AlgorithmChannel = "channels/ea711973-4c84-4916-9e1d-12ab22d19e66/messages/algorithm"
ImageBeforeProcessingPath = "./images/BeforeProcess.jpg"
ImageAfterProcessingPath = "./images/AfterProcess.jpg"
NoImagePath = "./images/No_image.png"

class ClientMQTT:
    GUI = None
    CheckOtherModules=True

    def __init__(self, GUI=None):
        self.GUI = GUI

        self.startReceiving()


    def on_message(self, client, userdata, message):
        self.GUI.addtoLog("message topic="+ message.topic, showOnGUI=False)
        self.GUI.addtoLog("message qos="+ str(message.qos), showOnGUI=False)
        self.GUI.addtoLog("message retain flag="+ str(message.retain), showOnGUI=False)
        try:
            s1 = str(message.payload.decode("utf-8"))
            y = json.loads(s1)
            print(s1)
            if y["cmd"] == "FromCamera":
                self.GUI.addtoLog("Receiving image from Camera" , color="red")
                self.GUI.filename = ImageBeforeProcessingPath
                with open(self.GUI.filename, "wb") as new_file:
                    new_file.write(base64.b64decode(y["img"]))
                    new_file.close()
                self.GUI.updatePlaceHolder1(filename=self.GUI.filename)
            if y["cmd"] == "Processed":
                self.GUI.filename = ImageAfterProcessingPath
                self.GUI.addtoLog("Receiving processed image from Algorithm Module", color="blue")
                with open(self.GUI.filename, "wb") as new_file:
                    new_file.write(base64.b64decode(y["img"]))
                    new_file.close()
                self.GUI.updatePlaceHolder2(filename=self.GUI.filename)
            if y["cmd"] == "msg":
                self.GUI.addtoLog(y["message"],color="green")

            self.startReceiving()
        except Exception as ex:
            print("Error:" + ex)

    def on_connect(self, client_1, userdata, flags, rc):
        if rc == 0:
            self.GUI.addtoLog("connected1 OK", showOnGUI=False)
        else:
            self.GUI.addtoLog("Bad connection Returned code="+ rc , showOnGUI=False)

    def startReceiving(self):
        self.GUI.addtoLog("creating new instance", showOnGUI=False)
        client = mqtt.Client("camera383")  # create new instance
        client._port = 1883
        client._username = username_interface
        client._password = password_interface
        client.on_message = self.on_message  # attach function to callback
        client._on_connect = self.on_connect
        client.connect(host)  # connect to broker
        self.GUI.addtoLog("Subscribing to topic", showOnGUI=False)
        client.subscribe(CameraChannel)
        client.subscribe(AlgorithmChannel)
        client.subscribe(InterfaceChannel)
        if self.CheckOtherModules:
            myData = {'cmd': "AreYouHere"}  # json
            auth1 = dict(username=username_interface, password=password_interface)
            client.publish(topic=InterfaceChannel, payload=json.dumps(myData))
            self.CheckOtherModules=False
        self.GUI.addtoLog("InterFace waiting ..........")
        time.sleep(1)
        client.loop_forever()  # start the loop


class MainWindow:
    master = None

    def __init__(self):
        self.master = tk.Tk()
        self.master.configure(bg='white')
        self.master.geometry("1280x700")
        self.master.title("Computer Vision Case")
        self.button = tk.Button(self.master, text="Capture From Cam", command=self.sendCaptureCommand)
        self.button.place(x=245, y=500, width=150, height=30)
        self.button = tk.Button(self.master, text="Process", command=self.sendProcessCommand)
        self.button.place(x=885, y=500, width=150, height=30)
        self.button = tk.Button(self.master, text="Open Local Image", command=self.openLocalImage)
        self.button.place(x=565, y=500, width=150, height=30)
        self.label1 = tk.Label(text="Capture or Upload Image")
        self.label1.place(x=0, y=0, width=640, height=480)
        self.label2 = tk.Label(text="No Proceessed Image yet")
        self.label2.place(x=640, y=0, width=640, height=480)
        self.LogsArea = tk.Text(self.master, height=1000, width=1000)
        self.S = tk.Scrollbar(self.LogsArea)
        self.S.pack(side=tk.RIGHT, fill=tk.Y)
        self.S.config(command=self.LogsArea.yview)
        self.LogsArea.config(yscrollcommand= self.S.set)
        self.LogsArea.place(x=0, y=550, width=1280, height=150)

        self.updatePlaceHolder1(filename=NoImagePath)
        self.updatePlaceHolder2(filename=NoImagePath)

        x = threading.Thread(target=ClientMQTT, args=(self,), daemon=True)
        x.start()
        self.master.mainloop()

    def convertImageToJson(self, filename):
        picId = ''.join(random.choice(string.ascii_lowercase) for i in range(8))
        with open(filename, mode='rb') as file:
            img = file.read()
        encoded = base64.encodebytes(img).decode("utf-8")
        myData = {'cmd': "Process", 'picId': picId, 'img': encoded}
        return myData

    def updatePlaceHolder1(self, filename):
        self.addtoLog("open file:: " + filename, color="red")
        image = Image.open(filename)
        image = image.resize((640, 480), Image.ANTIALIAS)
        image = ImageTk.PhotoImage(image)
        self.label1.configure(image=image, text="showing image")
        self.label1.image = image
        self.label1.text = "showing image"


    def updatePlaceHolder2(self, filename):
        self.addtoLog("open file:: " + filename, color="blue")
        image = Image.open(filename)
        image = image.resize((640, 480), Image.ANTIALIAS)
        image = ImageTk.PhotoImage(image)
        self.label2.configure(image=image, text="showing image")
        self.label2.image = image
        self.label2.text = "showing image"

    def addtoLog(self, text, showOnGUI=True,color="black"):
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        text = current_time + ":  " + r'{}'.format(text)
        print(text)
        if showOnGUI:
            text = text+"\n"
            self.LogsArea.insert(tk.END, text,color )
            self.LogsArea.see(tk.END)
            self.LogsArea.tag_configure(color, foreground=color)

    def openLocalImage(self):
        ff = filedialog.askopenfilename(initialdir="/", title="Select file",
                                        filetypes=(("jpeg files", "*.jpg"), ("all files", "*.*")))
        copyfile(ff, ImageBeforeProcessingPath)
        self.updatePlaceHolder1(filename=ImageBeforeProcessingPath)

    def sendCaptureCommand(self):
        self.updatePlaceHolder1(filename=NoImagePath)
        myData = {'cmd': "Capture"}  # json
        auth1 = dict(username=username_interface, password=password_interface)
        publish.single(topic=InterfaceChannel, payload=json.dumps(myData), hostname=host, port=1883, auth=auth1)
        self.addtoLog("InterFace send capture command")

    def on_publish(self, client, userdata, result):  # create function for callback
        self.addtoLog("data published \n")
        client.disconnect()
        pass

    def sendProcessCommand(self):
        self.updatePlaceHolder2(filename=NoImagePath)
        myData = self.convertImageToJson(ImageBeforeProcessingPath)  # json
        auth1 = dict(username=username_interface, password=password_interface)
        publish.single(topic=AlgorithmChannel, payload=json.dumps(myData), hostname=host, port=1883, auth=auth1)
        self.addtoLog("InterFace send process command")

MainWindow()
